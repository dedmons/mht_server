<!DOCTYPE html>
<html>
<head>
	<title>Test Page</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link href="./assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="./assets/css/custom.css" rel="stylesheet">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<!-- <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script> -->
	<script src="./assets/js/bootstrap.min.js"></script>
</head>
<body>
	<?php
	$jsonstring = file_get_contents("./application/config/prod_db_config.json");
  	$dbsettings = json_decode($jsonstring,true);
  	if (isset($dbsettings['hostname'])) {
  		header("Location: index.php/config/initalAccount");
  		exit;
  	}

  	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  		$jsonstring = file_get_contents("./application/config/prod_db_config.json");
		$jsonobj = json_decode($jsonstring,true);

		$jsonobj['hostname'] = $_POST['dbhost'];
		$jsonobj['username'] = $_POST['dbuser'];
		$jsonobj['password'] = $_POST['dbpass'];
		$jsonobj['database'] = $_POST['dbname'];
		$jsonobj['uname']    = $_POST['accountname'];
		$jsonobj['passwd']   = $_POST['accountpass'];

		$jsonstring = json_encode($jsonobj);

		file_put_contents("./application/config/prod_db_config.json", $jsonstring);

  		header("Location: index.php/config/initalAccount");
  		exit;
  	}
  	?>
	<div class="container-fluid">
		<div class="navbar">
			<div class="navbar-inner">
				<a class="brand" href="./">CU MHT</a>
				<ul class="nav">
					<!-- <li class="divider-vertical"></li> -->
					<li><a href="./index.php">Home</a></li>
					<!-- <li class="divider-vertical"></li> -->
					<li><a href="./index.php/config">Config</a></li>
				</ul>
			</div>
		</div>
		<div id="container">
			<div class="row-fluid">
				<div class="span6 offset3 text-center"><h2>Inital Server Settings</h2></div>
			</div>
			
				<form class="form-horizontal" id="dbform" method="post" action="./setup.php">
					<div class="row-fluid">
					<div class="span5 offset1">
						<div class="control-group">
							<label for="dbhost" class="control-label">Database Hostname</label>
							<div class="controlls">
								<input name="dbhost" type="text" placeholder="DB Host...">
							</div>
						</div>
						<div class="control-group">
							<label for="dbname" class="control-label">Database Name</label>
							<div class="controlls">
								<input name="dbname" type="text" placeholder="DB Name...">
							</div>
						</div>
						<div class="control-group">
							<label for="dbuser" class="control-label">Database Username</label>
							<div class="controlls">
								<input name="dbuser" type="text" placeholder="DB Username...">
							</div>
						</div>
						<div class="control-group">
							<label for="dbpass" class="control-label">Database Password</label>
							<div class="controlls">
								<input name="dbpass" type="password" placeholder="DB Pass...">
							</div>
						</div>
					</div>
					<div class="span5">
						<div class="control-group">
							<label for="accountname" class="control-label">Admin Account Name</label>
							<div class="controlls">
								<input name="accountname" type="text">
							</div>
						</div>
						<div class="control-group">
							<label for="" class="control-label">Admin Account Pass</label>
							<div class="controlls">
								<input name="accountpass" type="password">
							</div>
						</div>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span2 offset5">
						<div class="control-group">
							<label for="" class="control-label"></label>
							<div class="controlls">
								<button type="submit" class="btn">Submit</button>
							</div>
						</div>
					</div>
				</div>
				</form>
			
		</div>
		<div class="container-fluid">
			<hr>
			<div class="row-fluid">
				<div class="span9 offset1">
					<footer class="text-center">&copy; 2013 Douglas Edmonson &amp; Tyler McGough</footer>
				</div>
			</div>
			<hr>
		</div>
	</div>
</body>
</html>