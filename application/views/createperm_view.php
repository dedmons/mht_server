<div class="pull-center">
<form class="form-horizontal" id="useredit" method="post" action="<?php echo base_url('index.php/admin/createperm')?>">
	<div class="control-group">
		<label for="name" class="control-label">Permission Name</label>
		<div class="controlls">
			<input name="name" type="text" placeholder="Permission name">
		</div>
	</div>
	<div class="control-group">
		<label for="desc" class="control-label">Permission Description</label>
		<div class="controlls">
			<textarea name="desc"></textarea>
		</div>
	</div>
	<div class="control-group">
		<label for="" class="control-label"></label>
		<div class="controlls">
			<button type="submit" class="btn btn-primary">Create</button>
		</div>
	</div>
</form>
</div>