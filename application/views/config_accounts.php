<h3 class="text-center">Account Settings Page</h3>
<table class="table table-bordered userTable">
	<thead>
		<tr>
			<th>User ID</th>
			<th>Username</th>
			<th>Permissions</th>
			<th>Edit User</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($users as $user): ?> 
		<tr>
			<td class="userCell_id"><?php echo $user->usr_id; ?></td>
			<td class="userCell_norm"><?php echo $user->usr_uname; ?></td>
			<td class="userCell_norm">
				<?php 
				$perms = array();
				$res = $this->user_manager->get_all_perms_for_user($user->usr_id);
				if(!$res) $res = array();
				foreach ($res as $perm) {
					$perms[] = $perm->perm_name;
				} 
				echo join(', ',$perms);
				?>
			</td>
			<td class="userCell_norm">
				<div class="">
					<a href="<?php echo base_url("index.php/admin/edituser/id/".$user->usr_id); ?>">Info</a>
					|
					<a href="<?php echo base_url("index.php/admin/edituserperms/id/".$user->usr_id); ?>">Permissions</a>
					|
					<a href="<?php echo base_url("index.php/admin/deleteuser/id/".$user->usr_id); ?>">Delete</a>
				</div>
			</td>
		</tr>
	<?php endforeach; ?>
	<tr>
		<td colspan="2" class="userCell_double">
			<div class="pull-center">
				<a href="<?php echo base_url("index.php/admin/createuser"); ?>">Create New User</a>
			</div>
		</td>
		<td colspan="2" class="userCell_double">
			<div class="pull-center">
				<a href="<?php echo base_url("index.php/admin/editperms"); ?>">Edit Permissions</a>
			</div>
		</td>
	</tr>
</tbody>
</table>
