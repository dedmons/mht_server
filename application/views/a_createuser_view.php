<div class="pull-center">
	<form class="form-horizontal" id="useredit" method="post" action="<?php echo base_url('index.php/admin/createuser')?>">
		<div class="row-fluid">
			<div class="span6">
				<div class="control-group">
					<label for="username" class="control-label">User Name</label>
					<div class="controlls">
						<input name="username" type="text" placeholder="User name">
					</div>
				</div>
				<div class="control-group">
					<label for="rname" class="control-label">Real Name</label>
					<div class="controlls">
						<input name="rname" type="text" placeholder="Real Name">
					</div>
				</div>
				<div class="control-group">
					<label for="email" class="control-label">User Email</label>
					<div class="controlls">
						<input name="email" type="text" placeholder="User Email">
					</div>
				</div>
				<div class="control-group">
					<label for="newpass" class="control-label">New Password</label>
					<div class="controlls">
						<input name="newpass" type="password" placeholder="New Pass">
					</div>
				</div>
				<div class="control-group">
					<label for="confirmpass" class="control-label">Confirm Password</label>
					<div class="controlls">
						<input name="confirmpass" type="password" placeholder="Confirm Pass">
					</div>
				</div>
			</div>
			<div class="span6">
				<h4>Select New User Permissions</h4>
				<?php foreach ($perms as $perm): ?>
				<div class="control-group">
					<div class="controlls">
						<label class="checkbox">
							<input name="<?php echo $perm->perm_id; ?>" type="checkbox">
							<?php echo $perm->perm_name; ?>
						</label>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
		<div class="control-group">
			<label for="" class="control-label"></label>
			<div class="controlls">
				<button type="submit" class="btn btn-primary">Create</button>
			</div>
		</div>
	</form>
</div>