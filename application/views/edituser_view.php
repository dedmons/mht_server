<form class="form-horizontal" id="useredit" method="post" action="<?php echo base_url('index.php/admin/edituser/id/'.$userdata->usr_id)?>">
	<div class="control-group">
		<label for="username" class="control-label">User Name</label>
		<div class="controlls">
			<input name="username" type="text" placeholder="User name" value="<?php echo $userdata->usr_uname?>">
		</div>
	</div>
	<div class="control-group">
		<label for="rname" class="control-label">Real Name</label>
		<div class="controlls">
			<input name="rname" type="text" placeholder="Real Name" value="<?php echo $userdata->usr_rname?>">
		</div>
	</div>
	<div class="control-group">
		<label for="email" class="control-label">User Email</label>
		<div class="controlls">
			<input name="email" type="text" placeholder="User Email" value="<?php echo $userdata->usr_email?>">
		</div>
	</div>
	<div class="control-group">
		<label for="newpass" class="control-label">New Password</label>
		<div class="controlls">
			<input name="newpass" type="password" placeholder="New Pass">
		</div>
	</div>
	<div class="control-group">
		<label for="confirmpass" class="control-label">Confirm Password</label>
		<div class="controlls">
			<input name="confirmpass" type="password" placeholder="Confirm Pass">
		</div>
	</div>
	<div class="control-group">
		<label for="" class="control-label"></label>
		<div class="controlls">
			<button type="submit" class="btn">Submit</button>
		</div>
	</div>
</form>