<h3 class="text-center">Edit permissions</h3>
<table class="table table-bordered userTable">
	<thead>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Description</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($perms as $perm): ?>
		<tr>
			<td class="userCell_id"><?php echo $perm->perm_id; ?></td>
			<td class="userCell_norm"><?php echo $perm->perm_name; ?></td>
			<td class="userCell_norm"><?php echo $perm->perm_description; ?></td>
			<td class="userCell_norm">
				<div class="">
					<a href="<?php echo base_url("index.php/admin/deleteperm/id/".$perm->perm_id); ?>">Delete</a>
				</div>
			</td>
		</tr>
		<?php endforeach; ?>
		<tr>
			<td colspan="4">
				<a href="<?php echo base_url("index.php/admin/createperm"); ?>">Create Permission</a>	
			</td>
		</tr>
	</tbody>
</table>