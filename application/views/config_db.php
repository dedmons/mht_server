<div class="pull-center">
	<h2>Current DB Settings</h2>
	<pre class="pre-scrollable"><?php print_r($dbsettings); ?></pre>
	<h2>New Database Settings</h2>
	<form class="form-horizontal" id="dbform" method="post" action="<?php echo base_url('index.php/config')?>">
		<div class="control-group">
			<label for="dbhost" class="control-label">Database Hostname</label>
			<div class="controlls">
				<input name="dbhost" type="text" placeholder="DB Host...">
			</div>
		</div>
		<div class="control-group">
			<label for="dbname" class="control-label">Database Name</label>
			<div class="controlls">
				<input name="dbname" type="text" placeholder="DB Name...">
			</div>
		</div>
		<div class="control-group">
			<label for="dbuser" class="control-label">Database Username</label>
			<div class="controlls">
				<input name="dbuser" type="text" placeholder="DB Username...">
			</div>
		</div>
		<div class="control-group">
			<label for="dbpass" class="control-label">Database Password</label>
			<div class="controlls">
				<input name="dbpass" type="password" placeholder="DB Pass...">
			</div>
		</div>
		<div class="control-group">
			<label for="" class="control-label"></label>
			<div class="controlls">
				<button type="submit" class="btn">Submit</button>
			</div>
		</div>
	</form>
</div>