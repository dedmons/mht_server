<!-- <div class="tabbable"> -->
<div>
	<ul class="nav nav-tabs">
		<li class="<?php if($conftab === 1) echo 'active' ?>">
			<a href="<?php echo base_url('index.php/config/accountsettings')?>">Manage Accounts</a>
		</li>
		<li class="<?php if($conftab === 2) echo 'active' ?>">
			<a href="<?php echo base_url('index.php/config/dbsettings')?>">Database Settings</a>
		</li>
		<li class="<?php if($conftab === 3) echo 'active' ?>">
			<a href="<?php echo base_url('index.php/config/settings')?>">Settings</a>
		</li>
	</ul>
	<!-- <div class="tab-content"> -->
	<div>
		<!-- <div class="tab-pane"> -->
		<div>
			<?php
			if ($conftab === 1) $this->load->view('config_accounts');
			else if ($conftab === 2) $this->load->view('config_db');
			else if ($conftab === 3) $this->load->view('config_settings');
 			?>
		</div>
  </div>
</div>