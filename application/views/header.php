<!DOCTYPE html>
<html>
	<head>
		<title>Test Page</title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/css/custom.css') ?>" rel="stylesheet">
	</head>
	<body>
		<?php 
		// print_r( $this->session->all_userdata() );
		// echo '<br>';
		// print_r( $this->user->user_info );
		?>
		<div class="container-fluid">
			<div class="navbar">
				<div class="navbar-inner">
					<a class="brand" href="<?php echo base_url() ?>">CU MHT</a>
					<ul class="nav">
						<!-- <li class="divider-vertical"></li> -->
						<li class="<?php if(isset($homeactive)) echo 'active'; ?>"><a href="<?php echo base_url() ?>">Home</a></li>
						<!-- <li class="divider-vertical"></li> -->
						<li class="<?php if(isset($configactive)) echo 'active'; ?>"><a href="<?php echo base_url('index.php/config') ?>">Config</a></li>
					</ul>
					<ul class="nav pull-right">
						<?php
						if ($this->load->is_loaded('user')){ 
							// echo $this->user->get_id();
							if ($this->user->validate_session()) {
								echo '<li><p class="navbar-text">Welcome '.$this->user->get_uname().'</p></li>';
								echo '<li class="divider-vertical"></li>';
								echo '<li><a href="'.base_url('index.php/login/out').'">Logout</a></li>';
							}
							else {
								echo '<li class="divider-vertical"></li>';
								echo '<li><a href="'.base_url('index.php/login').'">Login</a></li>';
							}
						}
						?>
					</ul>
				</div>
			</div>
			<?php if(isset($error)): ?>
				<div class="row-fluid" id="error-div">
					<div class="span4 offset4">
						<div class="alert alert-error text-center">
							<button type="button" class="close error-btn-close">&times;</button>
							<strong>Error</strong>: <?php echo $error; ?>
						</div>	
					</div>
				</div>
			<?php endif; ?>
			<?php if(isset($success)): ?>
				<div class="row-fluid" id="success-div">
					<div class="span4 offset4">
						<div class="alert alert-success text-center">
							<button type="button" class="close success-btn-close">&times;</button>
							<strong>Success</strong>: <?php echo $success; ?>
						</div>	
					</div>
				</div>
			<?php endif; ?>

			<div id="container">