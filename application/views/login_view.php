<?php 
	$redir = "/";
	if (isset($redirect)) {
		$redir .= $redirect;
	}
?>
<form class="form-horizontal" id="loginform" method="post" action="<?php echo base_url('index.php/login/in'.$redir)?>">
	<div class="control-group">
		<label for="uname" class="control-label">User Name</label>
		<div class="controlls">
			<input name="uname" type="text" placeholder="User Name..." autofocus>
		</div>
	</div>
	<div class="control-group">
		<label for="pass" class="control-label">Password</label>
		<div class="controlls">
			<input name="pass" type="password" placeholder="Pass...">
		</div>
	</div>
	<div class="control-group">
		<label for="" class="control-label"></label>
		<div class="controlls">
			<button type="submit" class="btn btn-primary">Submit</button>
			<a class="btn" href="<?php echo base_url('index.php/login/create')?>">Create Account</a>
		</div>
	</div>
</form>