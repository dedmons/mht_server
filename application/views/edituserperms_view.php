
<div class="row-fluid">
	<div class="offset1">
		<h3>Select permissions for: <?php echo $userdata->usr_uname ?></h3>
		<form class="" id="useredit" method="post" action="<?php echo base_url('index.php/admin/edituserperms/id/'.$userdata->usr_id)?>">
			<?php foreach ($perms as $perm): ?>
			<div class="control-group">
				<div class="controlls">
					<label class="checkbox">
						<input name="<?php echo $perm->perm_id; ?>" type="checkbox" <?php if($this->user_manager->user_has_permission($userdata->usr_id,$perm->perm_id)) echo 'checked'; ?>>
						<?php echo $perm->perm_name; ?>
					</label>
				</div>
			</div>
			<?php endforeach; ?>

			<div class="control-group">
				<label for="" class="control-label"></label>
				<div class="controlls">
					<button type="submit" class="btn">Update</button>
				</div>
			</div>
		</form>
	</div>
</div>