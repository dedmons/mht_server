<?php defined('BASEPATH') OR exit('No direct script access allowed');

define('DONT_UPDATE_LOGIN', false);

define('PASSWORD_IS_HASHED', true);
define('PASSWORD_IS_NOT_HASHED', false);

class User
{
	private $ci;
	public $user_info = array();

	function __construct(){
		$this->ci =& get_instance();

		if (!isset($this->ci->db)){
			show_error("This library requires the db library to be loaded");
		}

		$this->check_and_setup_db();

		$this->ci->load->library(array('session','bcrypt'));

		$this->validate_session();
	}

	private function check_and_setup_db(){
		$perms_tbl = "CREATE TABLE `permissions` (
  				`perm_id` mediumint(10) NOT NULL AUTO_INCREMENT,
 				`perm_name` varchar(12) NOT NULL,
  				`perm_description` text NOT NULL,
  				PRIMARY KEY (`perm_id`),
  				UNIQUE KEY `perm_name` (`perm_name`)
				) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8";

		$user_tbl = "CREATE TABLE `users` (
  						`usr_id` mediumint(10) NOT NULL AUTO_INCREMENT,
  						`usr_rname` varchar(50) NOT NULL,
  						`usr_email` varchar(30) DEFAULT NULL,
  						`usr_uname` varchar(16) NOT NULL,
  						`usr_passwd` varchar(60) NOT NULL,
  						`usr_last_login` date DEFAULT NULL,
  						`usr_active` tinyint(1) NOT NULL DEFAULT '1',
  						PRIMARY KEY (`usr_id`)
					) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8";

		$usr_perms_tbl = "CREATE TABLE `users_permissions` (
							  `usr_id` mediumint(10) NOT NULL,
							  `perm_id` mediumint(10) NOT NULL,
							  KEY `usr_id` (`usr_id`),
							  KEY `perm_id` (`perm_id`),
							  CONSTRAINT `usr_perm_fk_2` FOREIGN KEY (`perm_id`) REFERENCES `permissions` (`perm_id`) ON DELETE CASCADE,
							  CONSTRAINT `usr_perm_fk_1` FOREIGN KEY (`usr_id`) REFERENCES `users` (`usr_id`) ON DELETE CASCADE
						) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

		if(!$this->ci->db->table_exists('permissions')){
			// echo "Creating perms table";
			$this->ci->db->query($perms_tbl);
		}

		if(!$this->ci->db->table_exists('users')){
			// echo "Creating user table";
			$this->ci->db->query($user_tbl);
		}

		if(!$this->ci->db->table_exists('users_permissions')){
			// echo "Creating user_perms table";
			$this->ci->db->query($usr_perms_tbl);
		}
	}

	function get_id(){
		if (isset($this->user_info->usr_id))
			return $this->user_info->usr_id;
		else
			return false;
	}

	function get_email(){
		if (isset($this->user_info->usr_email))
			return $this->user_info->usr_email;
		else
			return false;
	}

	function get_uname(){
		return $this->ci->session->userdata('uname');
	}

	function get_rname(){
		if (isset($this->user_info->usr_rname))
			return $this->user_info->usr_rname;
		else
			return false;
	}

	function on_invalid_session($destiny){
		if(!$this->validate_session()){
			$this->ci->session->set_flashdata('error_message', 'Invalid session.');
			redirect($destiny);
		}
	}

	function on_valid_session($destiny){
		if($this->validate_session()) {
			$this->ci->session->set_flashdata('error_message', '');
			redirect($destiny);
		}
	}

	function validate_session(){
		if($this->login($this->ci->session->userdata('uname'),
						$this->ci->session->userdata('pw'), 
						DONT_UPDATE_LOGIN,
						PASSWORD_IS_HASHED))
		{
			return true;
		}
		return false;
	}

	function login($uname, $passwd, $update_last_login = true, $hashed_password = false){
		$user_query = $this->ci->db->get_where('users', array('usr_uname'=>$uname));

		if($user_query->num_rows()==1){
			// get user from the database
			$user_query = $user_query->row();

			// checks if user is active or not
			if($user_query->usr_active === 0) return false;

			// validates hash
			$valid_password = false;

			if(	! $hashed_password ){
				// echo "not hashed<br/>";
				// print_r($user_query);
				$valid_password = $this->ci->bcrypt->compare($passwd, $user_query->usr_passwd);
			}
			else {
				// echo "hashed<br/>";
				$valid_password = ($user_query->usr_passwd === $passwd);
			}

			if($valid_password){
				$this->user_info = $user_query;

				$this->_load_permission($this->user_info->usr_id);

				$this->_create_session($uname, $user_query->usr_passwd);

				if($update_last_login)
					$this->update_last_login();

				return true;
			} else {
				// echo "bad pass<br/>";
				return false;
			}
		} else {
			// echo "bad user<br/>";
			return false;
		}
	}


	function match_password($password_string){
		return $this->ci->bcrypt->compare($password_string, $this->user_info->usr_passwd);
	}

	function update_last_login(){
		$this->ci->db->where(array('usr_id'=>$this->get_id()));
		return $this->ci->db->update('users', array('usr_last_login' => date('Y-m-d')));
	}

	function has_permission($permission_name){
		if( ! $this->ci->session->userdata('session_id')){
			return false;
		} else if (in_array($permission_name, $this->user_permission)) {
			return true;
		} else {
			return false;
		}
	}

	function update_pw($new_pw, $is_hashed = false){
		if (! $is_hashed) {
			$new_pw = $this->ci->bcrypt->hash($new_pw);
		}

		$this->ci->session->set_userdata(array('pw'=>$new_pw));
		$this->user_info->usr_passwd = $new_pw;

		$sts = $this->ci->db->update('users', array('usr_passwd'=>$new_pw));

		return $sts;
	}

	function set_pw($new_pw, $is_hashed = false){
		if (! $is_hashed) {
			$new_pw = $this->ci->bcrypt->hash($new_pw);
		}

		$this->ci->session->set_userdata(array('pw'=>$new_pw));
		$this->user_info->usr_passwd = $new_pw;

		return true;
	}

	function destroy_user(){
		$this->ci->session->set_userdata(array('uname'=>"", 'pw'=>""));
		$this->ci->session->sess_destroy();
		unset($this->user_info);
		return true;
	}

	private function _load_permission(){
		$permissions = $this->ci->db
		->join('users_permissions', 'users_permissions.perm_id = permissions.perm_id')
		->get_where('permissions', array('users_permissions.usr_id'=>$this->get_id()))
		->result();

		$user_permissions = array();

		foreach($permissions as $permission){
			$user_permissions[] = $permission->perm_name;
		}
		$this->user_permission = $user_permissions;
	}

	private function _create_session($uname, $passwd){
		$this->ci->session->set_userdata(array('uname'=>$uname, 'pw'=>$passwd));
	}
}

/* End of User.php */