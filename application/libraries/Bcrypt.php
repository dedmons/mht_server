<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bcrypt {
	private $times;
	private $random_state;

	function __construct($times = 12){
		$this->CI =& get_instance();

		// Bcrypt not supported
		if(CRYPT_BLOWFISH != 1) {
			show_error('Bcrypt is not installed or is not supported in this system.');
		}

		$this->times = $times;
	}

	function hash($input){
		$hash = crypt($input, $this->generate_salt());

		// hash untill successful
		while(strlen($hash) < 13)
			$hash = crypt($input, $this->generate_salt());

		return $hash;
	}

	public function compare($input, $hashed_string) {
		$hash = crypt($input, $hashed_string);
		// return if the hashed string is the same
		return $hash === $hashed_string;
	}


	function generate_salt(){
		$salt = sprintf('$2a$%02d$', $this->times);

		// generate random bytes for our salt
		$bytes = $this->get_random_bytes(16);

		$salt .= substr(base64_encode($bytes),0,-2);

		return $salt;
	}

	function get_random_bytes($count){
		$bytes = '';

		if(function_exists('openssl_random_pseudo_bytes')) {
			$bytes = openssl_random_pseudo_bytes($count);
		}

		if($bytes === '' && is_readable('/dev/urandom') && ($h_rand = @fopen('/dev/urandom', 'rb')) !== false){
			$bytes = fread($h_rand, $count);
			fclose($h_rand);
		}

		if(strlen($bytes) < $count) {
			$bytes = '';

			if($this->random_state === null) {
				$this->random_state = microtime();
				if(function_exists('getmypid')) {
					$this->random_state .= getmypid();
				}
			}


			for($i = 0; $i < $count; $i += 16) {
				$this->random_state = md5(microtime() . $this->random_state);
				$bytes .= md5($this->random_state, true);
			}

			$bytes = substr($bytes, 0, $count);
		}

		return $bytes;
	}
}