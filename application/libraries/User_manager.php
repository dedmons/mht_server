<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class User_manager {
	
	private $ci;

	function __construct(){
		$this->ci =& get_instance();
        
		if(!isset($this->ci->db)){
			show_error("You need the database library");
		}

		$this->ci->load->library(array('session', 'bcrypt'));
	}

	function save_user($real_name, $user_name, $passwd, $email, $active = 1, $permissions=array()) {        
		
		if( ! $this->login_exists($user_name) && $real_name!= "") {
	
			// generate the hashed password
			$hashed_password = $this->ci->bcrypt->hash($passwd);
	
			// This login is fine, proceed
			if ( $this->ci->db->insert('users', array('usr_rname'=>$real_name, 'usr_uname'=>$user_name, 'usr_email'=>$email, 'usr_passwd'=>$hashed_password, 'usr_active'=>$active)) ) {
                
				// Saved successfully
				$new_user_id = $this->ci->db->insert_id();

				// Add the permissions
				$this->add_permission($new_user_id, $permissions);

				// Return the new user id
				return $new_user_id;
			}
		} else {
			// Login already exists or full name is empty
			return false;
		}
	}

	function update_user($user_id, $updates, $hashed_password = false){
		if ( (! $hashed_password) && isset($updates['usr_passwd'])) {
			$updates['usr_passwd'] = $this->ci->bcrypt->hash($updates['usr_passwd']);
		}

		$this->ci->db->where('usr_id',$user_id);
		return $this->ci->db->update('users',$updates);
	}
    
	function delete_user($user_id){
		return $this->ci->db->delete('users', array('usr_id'=>$user_id));
	}

	function login_exists($user_name){
		$exists = $this->ci->db->get_where('users', array('usr_uname'=>$user_name))->row();
		return sizeof($exists) != 0;
	}

	function user_has_permission($user_id, $permission_id){
		$result = $this->ci->db->get_where('users_permissions', array('usr_id' => $user_id, 'perm_id' =>$permission_id));
		return ( $result->num_rows() == 1 );
	}

	function add_permission($user_id, $permissions) {
		// If array received we must call this recursively
		if(is_array($permissions)) {
			if(sizeof($permissions) == 0) {
				return FALSE;
			}
			// Foreach permission in the array call this function recursively
			foreach($permissions as $permission) {
				$this->add_permission($user_id, $permission);
			}
		} else {
			// Check if user already has this permission
			if( ! $this->user_has_permission($user_id, $permissions) ) {
				return $this->ci->db->insert('users_permissions', array('usr_id'=>$user_id, 'perm_id'=>$permissions));
			} else {
				// User already has this permission
				return TRUE;
			}
		}
	}

	function remove_permission($permid){
		return $this->ci->db->delete('permissions',array('perm_id'=>$permid));
	}

	function remove_user_permissions($userid){
		$this->ci->db->where('usr_id',$userid);
		$this->ci->db->delete('users_permissions');
	}

	// Creates a new permission
	function save_permission($permission_name, $permission_description){
		$exists = $this->ci->db->get_where('permissions', array('perm_name'=>$permission_name));
		if( $exists->num_rows() >= 1 ) {
			return $exists->row()->perm_id;
		} else { 
			$insert = $this->ci->db->insert('permissions', array('perm_name'=>$permission_name, 'perm_description'=>$permission_description));
			if( $insert ) {
				return $this->ci->db->insert_id();
			} else {
				return FALSE;
			}
		}
	}

	// Gets all users with a selected permission
	function get_users_with_permission($permission_name){
		$permission = $this->ci->db->get_where('permissions', array('perm_name'=>$permission_name))->row();
		if(sizeof($permission) == 0) {
			return FALSE;
		} else {
			return $this->ci->db->get_where('users_permissions', array('perm_id'=>$permission->id))->result();
		}
	}

	function get_all_users(){
		$users = $this->ci->db->get('users');
		if ($users->num_rows() === 0) {
			return FALSE;
		} else {
			return $users->result();
		}
	}

	function get_all_perms(){
		$perms = $this->ci->db->get('permissions');
		if ($perms->num_rows() === 0) {
			return FALSE;
		} else {
			return $perms->result();
		}
	}

	function get_all_perms_for_user($userid){
		$this->ci->db->from('permissions');
		$this->ci->db->join('users_permissions','permissions.perm_id = users_permissions.perm_id', 'left');
		$this->ci->db->where('users_permissions.usr_id',$userid);
		$perms = $this->ci->db->get();
		if ($perms->num_rows() === 0) {
			return FALSE;
		} else {
			return $perms->result();
		}
	}

	// // Add (and saves to database) a custom user information
	// function set_custom_field($user_id, $name, $value){
	// 	$field = $this->ci->db->get_where('users_meta', array('user_id'=>$user_id, 'name'=>$name));
	// 	if($field->num_rows() == 0){
	// 		return $this->db->insert('users_meta', array('user_id'=>$user_id, 'name'=>$name, 'value'=>$value));
	// 	} else {
	// 		return $this->db->update('users_meta', array('user_id'=>$user_id, 'name'=>$name, 'value'=>$value), array('user_id'=>$user_id));
	// 	}
	// }
	
	// // Add (and saves to database) a custom user information
	// function get_custom_field($user_id, $name, $value){
	// 	$field = $this->ci->db->get_where('users_meta', array('user_id'=>$user_id, 'name'=>$name));
	// 	if($field->num_rows() == 0){
	// 		return FALSE;
	// 	} else {
	// 		return $field->row()->value;
	// 	}
	// }

}
