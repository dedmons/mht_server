<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
|				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = '';
$active_record = TRUE;

// $db['localdev']['hostname'] = 'localhost';
// $db['localdev']['username'] = 'root';
// $db['localdev']['password'] = 'root';
// $db['localdev']['database'] = 'mht_db_dev';
// $db['localdev']['dbdriver'] = 'mysql';
// $db['localdev']['dbprefix'] = '';
// $db['localdev']['pconnect'] = TRUE;
// $db['localdev']['db_debug'] = TRUE;
// $db['localdev']['cache_on'] = FALSE;
// $db['localdev']['cachedir'] = '';
// $db['localdev']['char_set'] = 'utf8';
// $db['localdev']['dbcollat'] = 'utf8_general_ci';
// $db['localdev']['swap_pre'] = '';
// $db['localdev']['autoinit'] = TRUE;
// $db['localdev']['stricton'] = FALSE;

if(file_exists("./application/config/prod_db_config.json")){
  // log_message('info',"file exists");

  $jsonstring = file_get_contents("./application/config/prod_db_config.json");
  // log_message('info',"JSON DATA: ".$jsonstring);
  $dbsettings = json_decode($jsonstring,true);

  $db['prod']['hostname'] = $dbsettings['hostname'];
  $db['prod']['username'] = $dbsettings['username'];
  $db['prod']['password'] = $dbsettings['password'];
  $db['prod']['database'] = $dbsettings['database'];
  $db['prod']['dbdriver'] = 'mysql';
  $db['prod']['dbprefix'] = '';
  $db['prod']['pconnect'] = TRUE;
  $db['prod']['db_debug'] = TRUE;
  $db['prod']['cache_on'] = FALSE;
  $db['prod']['cachedir'] = '';
  $db['prod']['char_set'] = 'utf8';
  $db['prod']['dbcollat'] = 'utf8_general_ci';
  $db['prod']['swap_pre'] = '';
  $db['prod']['autoinit'] = TRUE;
  $db['prod']['stricton'] = FALSE;

  $active_group = 'prod';
  
} else {
  // log_message('info',"file does not exist");
}


/* End of file database.php */
/* Location: ./application/config/database.php */
