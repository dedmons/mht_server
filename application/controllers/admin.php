<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require(APPPATH.'/libraries/REST_Controller.php');

class Admin extends REST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->user->on_invalid_session('login/redir/config');

		if (!$this->user->has_permission("admin")) {
			redirect('login');
		}
	}

	public function index_get(){
		redirect('/');
	}

	public function edituser_get(){
		$this->db->from('users');
		// $this->db->join('users_permissions','users.usr_id = users_permissions.usr_id', 'left');
		// $this->db->join('permissions', 'permissions.perm_id = users_permissions.perm_id', 'left');
		$this->db->where('users.usr_id',$this->get('id'));

		$query = $this->db->get();

		$viewdata = array(
			'content'=>"edituser_view",
			'userdata'=>$query->row()
			);
		$this->load->view("standard_view",$viewdata);
	}

	public function edituser_post(){
		$updates = array();
		if ($this->post('username'))
			$updates['usr_uname'] = $this->post('username');

		if ($this->post('rname'))
			$updates['usr_rname'] = $this->post('rname');

		if ($this->post('email'))
			$updates['usr_email'] = $this->post('email');

		if ($this->post('newpass') && $this->post('confirmpass')){
			if ( $this->post('newpass') === $this->post('confirmpass') ) {
				$newpass = $this->bcrypt->hash($this->post('newpass'));
				echo $newpass."<br/>";
				$updates['usr_passwd'] = $newpass;
				if ($this->get('id') === $this->user->get_id()) {
					$this->user->set_pw($newpass,true);
				}
			} else {
				$updates['usr_id'] = $this->get('id');
				$viewdata = array(
					'content'=>"edituser_view",
					'userdata'=>(object)$updates,
					'error'=>"Passwords Do Not Match!"
					);
				$this->load->view("standard_view",$viewdata);
				return;
			}
		}

		$sts = $this->user_manager->update_user($this->get('id'),$updates, true);
		if ($sts) {
			$this->session->set_flashdata('success','Changes have been saved!');
		} else {
			$this->session->set_flashdata('error','Error saving changes!');
		}
		// print_r($updates);
		redirect('config/accountsettings');
	}

	public function edituserperms_get(){
		$this->db->from('users');
		$this->db->where('users.usr_id',$this->get('id'));

		$query = $this->db->get();

		$perms = $this->user_manager->get_all_perms();

		$viewdata = array(
			'content'=>"edituserperms_view",
			'userdata'=>$query->row(),
			'perms'=> $perms
			);
		$this->load->view("standard_view",$viewdata);
	}

	public function edituserperms_post(){
		$this->user_manager->remove_user_permissions($this->get('id'));

		$newperms = array();
		foreach ($this->post() as $key => $value) {
			if ($value === "on") {
				$newperms[] = $key;
			}
		}
		
		$sts = $this->user_manager->add_permission($this->get('id'),$newperms);
		if ($sts !== false || count($newperms) === 0) {
			$this->session->set_flashdata('success','Changes have been saved!');
		} else {
			$this->session->set_flashdata('error','Error saving changes!');
		}
		redirect('config/accountsettings');
	}

	public function createuser_get(){
		$viewdata = array(
			'content'=> "a_createuser_view",
			'perms' => $this->user_manager->get_all_perms()
			);

		if ($this->session->flashdata('success')) {
			$viewdata['success'] = $this->session->flashdata('success');
		} else if ($this->session->flashdata('error')) {
			$viewdata['error'] = $this->session->flashdata('error');
		}

		$this->load->view("standard_view",$viewdata);
	}

	public function createuser_post(){
		
		if($this->post('newpass') === $this->post('confirmpass')){
			if($this->post('newpass') === ""){
				$this->session->set_flashdata('error','Passwords cannot be empty!');
				redirect('admin/createuser');
			} else {
				$newperms = array();
				foreach ($this->post() as $key => $value) {
					if ($value === "on") {
						$newperms[] = $key;
					}
				}

				$sts = $this->user_manager->save_user($this->post('rname'), $this->post('username'),
					$this->post('newpass'), $this->post('email'),1,$newperms);
				if($sts !== FALSE)
					$this->session->set_flashdata('success','Changes have been saved!');
				else
					$this->session->set_flashdata('error','Error creating user!');
				redirect('config/accountsettings');
			}
		} else {
			$this->session->set_flashdata('error','Passwords do not match!');
			redirect('admin/createuser');
		}
	}

	public function createperm_get(){
		$viewdata = array(
			'content'=> "createperm_view"
		);

		$this->load->view("standard_view",$viewdata);
	}

	public function createperm_post(){
		$sts = $this->user_manager->save_permission($this->post('name'), $this->post('desc'));
		if ($sts) {
			$this->session->set_flashdata('success','Changes have been saved!');
		} else {
			$this->session->set_flashdata('error','Error saving changes!');
		}
		redirect('config/accountsettings');
	}

	public function editperms_get(){
		$viewdata = array(
			'content'=> "editperms_view",
			'perms'=>$this->user_manager->get_all_perms()
		);

		$this->load->view("standard_view",$viewdata);
	}

	public function deleteperm_get(){
		if ($this->get('id')) {
			$sts = $this->user_manager->remove_permission($this->get('id'));
			if($sts)
				$this->session->set_flashdata('success','Permission deleted');
			else
				$this->session->set_flashdata('error','Problem deleting permission');
			redirect('config/accountsettings');
		}
	}

	public function deleteuser_get(){
		if ($this->get('id')) {
			if($this->user->get_id() === $this->get('id')){
				$this->session->set_flashdata('error','Cannot delete yourself!');
				redirect('config/accountsettings');
			} else {
				$sts = $this->user_manager->delete_user($this->get('id'));
				if($sts)
					$this->session->set_flashdata('success','User deleted');
				else
					$this->session->set_flashdata('error','Problem deleting user');
				redirect('config/accountsettings');
			}
		}
	}
}