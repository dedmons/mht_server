<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require(APPPATH.'/libraries/REST_Controller.php');

class Config extends REST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->user->on_invalid_session('login/redir/config');

		if (!$this->user->has_permission("admin")) {
			redirect('login');
		}
	}

	public function index_get()
	{
		// $viewdata = array(
		// 	'content'=>"config_view",
		// 	'configactive'=>true,
		// 	'conftab'=>0
		// );
		// $this->load->view("standard_view",$viewdata);
		redirect("config/accountsettings");
	}

	public function index_post()
	{
		$this->load->helper('file');

		$jsonstring = file_get_contents("./application/config/prod_db_config.json");
		$jsonobj = json_decode($jsonstring,true);

		$jsonobj['hostname'] = $this->post('dbhost');
		$jsonobj['username'] = $this->post('dbuser');
		$jsonobj['password'] = $this->post('dbpass');
		$jsonobj['database'] = $this->post('dbname');

		$jsonstring = json_encode($jsonobj);

		$fres = "";

		// file_put_contents("./application/config/prod_db_config.json",$jsonstring);
		if ( ! write_file('./application/config/prod_db_config.json', $jsonstring) )
			$fres .= 'Unable to write the file';
		else
			$fres .= 'File written!';

		// redirect("/");
		$viewdata = array(
			'content'=>"config_view",
			'configactive'=>true,
			'dbsettings' => $fres
		);
		$this->load->view("standard_view",$viewdata);
	}

	public function dbsettings_get()
	{
		$jsonstring = file_get_contents("./application/config/prod_db_config.json");
		$viewdata = array(
			'content'=>"config_view",
			'configactive'=>true,
			'conftab'=>2,
			'dbsettings' => json_decode($jsonstring,true)
		);
		$this->load->view("standard_view",$viewdata);
	}

	public function accountsettings_get()
	{
		$this->db->select('users.usr_id,users.usr_uname');
		$this->db->from('users');

		$query = $this->db->get();

		$viewdata = array(
			'content'=>"config_view",
			'configactive'=>true,
			'conftab'=>1,
			'users'=>$query->result()
		);

		if ($this->session->flashdata('success')) {
			$viewdata['success'] = $this->session->flashdata('success');
		} else if ($this->session->flashdata('error')) {
			$viewdata['error'] = $this->session->flashdata('error');
		}

		$this->load->view("standard_view",$viewdata);
	}

	public function settings_get()
	{
		$viewdata = array(
			'content'=>"config_view",
			'configactive'=>true,
			'conftab'=>3
		);
		$this->load->view("standard_view",$viewdata);
	}

	public function dbsettings_post()
	{
		$this->load->helper('file');

		$jsonstring = file_get_contents("./application/config/prod_db_config.json");
		$jsonobj = json_decode($jsonstring,true);

		$jsonobj['hostname'] = $this->post('dbhost');
		$jsonobj['username'] = $this->post('dbuser');
		$jsonobj['password'] = $this->post('dbpass');
		$jsonobj['database'] = $this->post('dbname');

		$jsonstring = json_encode($jsonobj);

		$fres = "";

		// file_put_contents("./application/config/prod_db_config.json",$jsonstring);
		if ( ! write_file('./application/config/prod_db_config.json', $jsonstring))
		{
			$fres .= 'Unable to write the file';
		}
		else
		{
			$fres .= 'File written!';
		}

		// redirect("/");
		$viewdata = array(
			'content'=>"config_view",
			'configactive'=>true,
			'dbsettings' => $fres
		);
		$this->load->view("standard_view",$viewdata);
	}

	public function initalAccount_get()
	{
		$jsonstring = file_get_contents("./application/config/prod_db_config.json");
		$jsonobj = json_decode($jsonstring,true);

		if(isset($jsonobj['uname'])){
			$this->load->helper('file');

			$this->load->library('user_manager');
			$permID = $this->user_manager->save_permission("admin","system admins");
			$this->user_manager->save_user("Default Admin",$jsonobj['uname'],$jsonobj['passwd'],"",1,array($permID));
			unset($jsonobj['uname']);
			unset($jsonobj['passwd']);

			$jsonstring = json_encode($jsonobj);
			write_file('./application/config/prod_db_config.json', $jsonstring);
		}
		redirect("/");
	}

}

/* End of file config.php */
/* Location: ./application/controllers/config.php */