<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require(APPPATH.'/libraries/REST_Controller.php');

class Login extends REST_Controller {

	public function index_get()
	{
		$viewdata = array(
			'content'=> "login_view",
			'redirect'=> $this->_get_redirect_path()
		);
		$this->load->view("standard_view",$viewdata);
	}

	public function create_get(){
		$viewdata = array(
			'content'=> "createuser_view"
		);
		$this->load->view("standard_view",$viewdata);
	}

	public function error_get()
	{
		$viewdata = array(
			'content'=>"login_view",
			'error'=>"Bad Login Information"
		);
		$this->load->view("standard_view",$viewdata);
	}

	public function in_post()
	{
		$user = $this->post('uname');
		$pass = $this->post('pass');

		if(isset($user) && isset($pass)){
			print_r($this->post());
			echo "<br/>";
			if($this->user->login($user,$pass)){
				redirect($this->_get_redirect_path());
			} else {
				redirect("/login/error");
				// echo "error<br/>";
			}
		}
	}

	public function out_get()
	{
		$this->user->destroy_user();
		redirect("/");
	}

	private function _get_redirect_path(){
		$redirPath = "/";
		$segnum = $this->uri->total_segments();

		if ($segnum > 2) {
			$redirPath = "";
			for ($i=3; $i < $segnum; $i++) {
				$redirPath .= $this->uri->slash_segment($i);
			}
			$redirPath .= $this->uri->segment($segnum);
		}

		return $redirPath;
	}
}